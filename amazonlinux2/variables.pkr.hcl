variable "ami_name_prefix" {
  type    = string
  default = "gocd-server"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "source_ami" {
  type    = string
  default = "ami-063d4ab14480ac177"
}

variable "subnet_id" {
  type    = string
  default = "subnet-8909c3fe"
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}
