packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "amazon-linux-2" {
  ami_name      = "${var.ami_name_prefix}-${local.timestamp}"
  instance_type = var.instance_type
  region        = var.region
  source_ami    = var.source_ami
  ssh_username  = "ec2-user"
  subnet_id     = var.subnet_id
}

build {
  sources = [
    "source.amazon-ebs.amazon-linux-2",
  ]
  provisioner "ansible" {
    playbook_file = "./playbook.yml"
    extra_arguments = [
      "-v",
    ]
  }
}
